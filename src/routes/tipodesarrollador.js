const express = require("express");
const TDSchema = require('../models/TipoDesarrollador')
const sesionSchema = require("../models/sesion")
const router = express.Router();

router.post('/TD',(req,res)=>{
    // res.send("Usuario creado")

        const nuevatecnologia={
            "Tecnologia" : req.body.Tecnologia
            
        }

        sesionSchema
        .findOne({ Sesion: 1 })
        .then((data) => {
            const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
            const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
            console.log("Sesion: ", VSesion)
            console.log("Tipo: ", VTipo)

            if (VTipo == 1 || VTipo == 2) {

                try {
                    const Tec = TDSchema(nuevatecnologia);
                    console.log(Tec)
                    Tec
                    .save()
                    .then((data)=>{
                        res.json({mensaje:"ingresado"})
                    }).catch((error)=>{
                        res.json({mensaje: "duplicado"})
                    })
    

                } catch (error) {
                    console.log(error)
                    res.json({ Mensaje: "error" })
                }
            }
            else {
                res.json({ Mensaje: "No tienes acceso a esta acción" })
            }

        })
        .catch((error) => {
            res.json({ mensaje: "Sesion no iniciada" })
        })


})


router.get('/TD', async (req, res) => {

    const TEC = await TDSchema.find()
    sesionSchema
        .findOne({ Sesion: 1 })
        .then((data) => {
            const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
            const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
            console.log("Sesion: ", VSesion)
            console.log("Tipo: ", VTipo)

            if (VTipo == 1 || VTipo == 2) {

                try {

                    res.json(TEC)
                } catch (error) {
                    console.log(error)
                    res.json({ Mensaje: "error" })
                }
            }
            else {
                res.json({ Mensaje: "No tienes acceso a esta acción" })
            }
        })
        .catch((error) => {
            res.json({ mensaje: "Sesion no iniciada" })
        })
})


   


router.put('/TD', (req,res)=>{
   

    sesionSchema
    .findOne({ Sesion: 1 })
    .then((data) => {
        const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
        const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]


        if (VTipo == 1 || VTipo == 2) {

            try {
                console.log(req.body.TecnologiaV)
                console.log(req.body.TecnologiaN)
                TDSchema
                    .findOneAndUpdate({Tecnologia: req.body.TecnologiaV}, {Tecnologia: req.body.TecnologiaN})
                    .then((data) => {
                        console.log(data)
                        if(data==null){
                            res.json({ Mensaje: "Tecnologia no encontrado" })
                        }
                        else{
                            res.json({ Mensaje: "Tecnologia editada con exito" })
                        }
                        
                    }).catch((error) => {
                        console.log(error)
                        res.json({ Mensaje: "Error" })
                    })
                

            } catch (error) {
                console.log(error)
                res.json({ Mensaje: "Tecnologia no encontrado" })
            }
        }
        else {
            res.json({ Mensaje: "No tienes acceso a esta acción" })
        }

    })
    .catch((error) => {
        res.json({ mensaje: "Sesion no iniciada" })
    })


})



router.delete('/TD',(req,res)=>{
    // res.send("Usuario Eliminado")
     
   
    sesionSchema
    .findOne({ Sesion: 1 })
    .then((data) => {
        const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
        const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
        console.log("Sesion: ", VSesion)
        console.log("Tipo: ", VTipo)
        
        if (VTipo == 1) {

            try {
                TDSchema
                .findOneAndDelete({Tecnologia: req.body.Tecnologia})
                .then((data)=>{
                    if(data==null){
                        res.json({ Mensaje: "Tecnologia no encontrado" })
                    }
                    else{
                        res.json({ Mensaje: "Tecnologia eliminada con exito" })
                    }
                }).catch((error)=>{
                    console.log(error)
                    res.json({Mensaje:"Error1"})
                })

            } catch (error) {
                console.log(error)
                res.json({ Mensaje: "Error2" })
            }
        }
        else {
            res.json({ Mensaje: "No tienes acceso a esta acción" })
        }
    })
    .catch((error) => {
        res.json({ mensaje: "Sesion no iniciada" })
    })
})




///////////////////////////////////////////////////////////////////////////////////////////////////////








module.exports = router;
