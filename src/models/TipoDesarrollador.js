const mongoosedb = require('mongoose');
const SchemaDesarrollador =  new mongoosedb.Schema ({
    Tecnologia:{
        type:String, 
        unique:true
    }
  });
  const DesarrolladorModel = mongoosedb.model('tipo_desarrollador', SchemaDesarrollador); 
  module.exports = DesarrolladorModel;