const mongoosedb = require('mongoose');
const SchemaTipoPersonal =  new mongoosedb.Schema ({
    Tipo:{
        type:String ,
        unique:true
    }
  });
  const TipoPersonaldorModel = mongoosedb.model('tipo_personal', SchemaTipoPersonal); 
  module.exports = TipoPersonaldorModel;