const mongoosedb = require('mongoose');
const SchemaSesion =  new mongoosedb.Schema ({

    Sesion: {
        type:Number,
        unique:true
    },

    Tipo_Personal:{
        type:Number
    }

  });
  const SesionModel = mongoosedb.model('sesion', SchemaSesion); 
  module.exports = SesionModel;