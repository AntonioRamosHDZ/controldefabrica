const mongoosedb = require('mongoose');
const SchemaHorario =  new mongoosedb.Schema ({
    Usuario:{
        type:String,
        unique:true 
    },
    Entrada: {
        type:String
    },
    Salida: {
        type:String    
    }
  });
  const HorarioModel = mongoosedb.model('horario', SchemaHorario); 
  module.exports = HorarioModel;