const express = require('express')
const mongoose = require('mongoose')
require("dotenv").config();



const rutasBitacora= require('./src/routes/bitacora')
const rutasHorario= require('./src/routes/horario')
const {router}= require('./src/routes/personal')
const rutasPersonal = router

// const rutasTipoP=require('./src/routes/tipopersonal')
const rutasTipoD=require('./src/routes/tipodesarrollador')


const app= express()
const port = 3050

//middleware
app.use(express.json())
app.use('/api', rutasPersonal);
app.use('/api', rutasBitacora); //mapear
app.use('/api', rutasHorario);
app.use('/api', rutasTipoD);
// app.use('/api', rutasTipoP);

//routes
app.get('/', (req,res)=> res.send('Hello World!'))

mongoose.connect(process.env.MONGODB_URI)
.then(()=>{
    console.log("La base de datos se conecto correctamente")
}).catch(e=>{
    console.log(e)
    console.log("Hubo un error")
})

app.listen(port,() => console.log(`Example app listening on port ${port}!`))